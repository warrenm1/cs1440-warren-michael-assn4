import sys
from pathlib import Path

images = {}

#input from terminal
#allows .frac file to be sent from anywhere on the system, as long as location is specified
def config():
    if len(sys.argv) < 2:
        print("Unable to continue")
        print("Please use the following format:")
        print("python {initial file} {path to .frac file} {.frac file}")
        sys.exit()
    if len(sys.argv) < 3:
        print("Unable to continue")
        print("Please use the following format:")
        print("python {initial file} {path to .frac file} {.frac file}")
        sys.exit()
    sys.argv[2] = sys.argv[2].lower()

    if sys.argv[2].endswith(".frac"):
        sys.argv[2] = sys.argv[2][:-5]

    if sys.argv[1].endswith('/'):
        sys.argv[1] = sys.argv[1][:-1]

    if Path(sys.argv[1] + "/" + sys.argv[2] + ".frac").is_file():
        with open(sys.argv[1] + "/" + sys.argv[2] + ".frac") as file:
            for line in file:
                if line.startswith("#") or line == '\n':
                   continue
                else:
                    line = line.rstrip()
                    fields = line.split(':')
                    fields[0] = fields[0].lower()
                    fields[1] = fields[1].lower()

                    images[fields[0]] = fields[1]
            images["imagename"] = sys.argv[2]
    else:
        print("Please make sure your spelling is correct!")
        sys.exit()

    checkDirectives(images)
    return images

#Basic checking of directory
def checkDirectives(images):
    if ('pixels' not in images) or ('type' not in images) or ('centerx' not in images) or ('centery' not in images) or ('iterations' not in images) or ("axislength" not in images):
        print("Make sure the Config.frac file is setup correctly")
        sys.exit()
    if (images['type'] == 'julia') and (('creal' not in images) or ("cimag" not in images)):
        print("Make sure the Config.frac file is setup correctly")
        sys.exit()
    if images['type'] != ' julia' and images['type'] != ' mandelbrot':
        print(images['type'])
        print("Can only configure Julia or Mandelbrot Fractals")
        sys.exit()
    if not isinstance(images['pixels'], str) or not float(images['pixels']).is_integer():
        print("Pixels can only be written as an int")
        sys.exit()
    if not isinstance(images['centerx'], str) or not (is_float(images['centerx']) or float(images['centerx']).is_integer()) or not isinstance(images['centery'], str) or not (is_float(images['centery']) or float(images['centry']).is_integer()) or not isinstance(images['axislength'], str) or not (is_float(images['axislength']) or float(images['axislength']).is_integer()):
        print("CenterX, CenterY, and axisLength can only be expressed as an int or float")
        sys.exit()
    if images['type'] == ' julia' and not (isinstance(images['creal'], str) or isinstance(images['cimag'], str) or is_float(images['creal']) or is_float(images['cimag']) or float(images['creal']).is_integer() or float(images['cimag']).is_integer()):
        print("cReal and cImag can only be expressed as an int or float")
        sys.exit()
    if not isinstance(images['iterations'], str) or not float(images['iterations']).is_integer():
        print("Iterations can only be expressed as an int")
        sys.exit()

#Taken from https://stackoverflow.com/questions/35808484/how-to-check-if-a-string-represents-a-float-number
def is_float(string):
    try:
        return float(string) and '.' in string
    except ValueError:
        return False