#!/usr/bin/python3

## Julia Set Visualizer

from Config import config
from Gradient import gradient

images = config()

#Given a coordinate in the complex plane,
#return the iteration count of the Julia function at that point.
def juliaColor(z):
    """Return the color of the current pixel within the Julia set"""
    # TODO: I understand that you can get more 'interesting' images when you
    #       use a different value for c.  Maybe I can encode this into the
    #       image configuration dictionary down below...
    c = complex(float(images['creal']), float(images['cimag']))  # c0
    max_iterations = len(gradient) # call Gradient to get this number

    for i in range(max_iterations):
        z = z * z + c  # Get z1, z2, ...
        if abs(z) > 2:
            return i  # The sequence is unbounded
    return max_iterations - 1  # Indicate a bounded sequence
