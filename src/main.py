#!/usr/bin/python3

#Driver Program.
#Imports modules, accepts user's input user and calls functions from modules
#as appropriate to both display on-screen and write to disk a fractal image.

## Mandelbrot Set Visualizer

import sys
from Config import config
from ImagePainter import paint

images = config()

paint(images)

# Add more parameters to this dictionary to create new Mandelbrot images!
#
# Get some ideas from https://atopon.org/mandel/
# or https://sciencedemos.org.uk/mandelbrot.php
#
# Add more parameters to this dictionary to create new Julia pictures!
#
# Get some ideas from http://bl.ocks.org/syntagmatic/3736720

# TODO: write a small helper program to convert the websites'
#       (minX, minY), (maxX, maxY) coordinates into my
#       (centerX, centerY) + axisLength scheme