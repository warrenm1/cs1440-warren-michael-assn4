# Mandelbrot User Manual 
A routine that is used to implement a Mandelbrot or Julia Fractal based on a modifiable Config file.

**Routine Name**           main.py

**Author:** Michael A. Warren

**Language:** Python. This code can be used using a python compiler(python)

For example,

    python src/main.py {path to .frac file} {.frac file name}


**Description/Purpose:** This module has been refractored to account for growth and not hard-coded values, while effectively implementing the exact same results as the original code

**Input:** 
    
    {path to .frac file} -a valid path to the .frac file. 

-DO NOT include the .frac file name in this path!

-Program ends if the file doesn't exist. 

-Don't need to end in '/', but that is ok

-Reference starts from current directory. 

-No need for './' at the start of the path

    {.frac file name} - a valid .frac config file
	
-Make sure the format is the same as the other files

-Case Insensitive

**Output:** Display of the Fractal in a new window. 
	Fractal is then saved to folder the .frac file was in

**Writing a Config File**

Mandelbrot example:

    type: mandelbrot
    pixels: 640
    centerX:     -1.543577002
    centerY:     -0.000058690069
    axisLength:  0.000051248888
    Iterations: 100

-Type must say "mandelbrot", not case sensitive

-Pixels must be an integer

-CenterX, CenterY, and Axis Length must be an integer or float

-Iterations must be an integer

-Must have at least one space on the right side of the ':'

-Each element must have its own line, and be written on one line

-Anything you want to add to the file that isn't in the following format:

    {label}: {value}

must have a '#' at the beginning of the line


Julia example:

    type: Julia
    cReal: -1
    cImag: 0
    pixels: 1024
    centerX: 0.618
    centerY: 0.0
    axisLength: 0.017148277367054
    iterations: 78
    
Main differences from Mandelbrot:

-Type must say "Julia", case insensitive

-cReal and cImag can be either an integer or a floating point number

**Last Modified:** November/2018