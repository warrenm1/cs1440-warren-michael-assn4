import sys
from tkinter import Tk, PhotoImage, Canvas, mainloop
from mandelbrot import mandelbrotColor
from julia import juliaColor
from Gradient import gradient
from Config import images

window = Tk()
img = PhotoImage(width=images['pixels'],height=images['pixels'])

def paint(fractal):
    """Paint a Fractal image into the TKinter PhotoImage canvas.
    This function displays a really handy status bar so you can see how far
    along in the process the program is."""

    # Figure out how the boundaries of the PhotoImage relate to coordinates on
    # the imaginary plane.
    minx = Min(fractal['centerx'],fractal['axislength'])
    maxx = Max(fractal['centerx'],fractal['axislength'])
    miny = Min(fractal['centery'],fractal['axislength'])
    maxy = Max(fractal['centery'],fractal['axislength'])

    pixels = int(images['pixels'])
    pixelsize = abs(maxx - minx) / pixels

    portion = int(pixels / 64)
    for col in range(pixels):
        if col % portion == 0:
            # Update the status bar each time we complete 1/64th of the rows
            pips = col // portion
            pct = col / pixels
            print(f"{images['imagename']} ({pixels}x{pixels}) {'=' * pips}{'_' * (64 - pips)} {pct:.0%}", end='\r', file=sys.stderr)
        for row in range(pixels):
            x = minx + col * pixelsize
            y = miny + row * pixelsize
            color = gradColor(x,y)
            img.put(color, (col, row))
    print(f"{images['imagename']} ({pixels}x{pixels}) ================================================================ 100%",
          file=sys.stderr)
    display(pixels)

def gradColor(x,y):
    if images['type'] == ' mandelbrot':
        return gradient[mandelbrotColor(complex(x,y))]
    elif images['type'] == ' julia':
        return gradient[juliaColor(complex(x,y))]

def Min(center,axislength):
    return float(center) - (float(axislength) / 2.0)

def Max(center,axislength):
    return float(center) + (float(axislength) / 2.0)

def display(pixels):
    # Display the image on the screen
    canvas = Canvas(window, width=pixels, height=pixels, bg=gradient[0])
    canvas.pack()
    canvas.create_image((pixels/2, pixels/2), image=img, state="normal")
    saveImage()


# Save the image as a PNG
# TODO: I have heard that you can create pictures in other formats, such as GIF
#       and PPM.  I wonder how I do that?
def saveImage():
    img.write(images['imagename'] + ".png")
    print(f"Wrote image {images['imagename']}.png")
    mainloop()

