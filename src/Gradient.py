#Generates a color gradient of N steps, where the value of N
## is equal to the number of Iterations specified in the configuration file.

import Color



# This color gradient contains 78 color steps.  It would be nice if I
# could add more or different colors to this list, but it's so much work to
# calculate all of the in-between shades!
#grad = ['#ffffff', '#ff00ff', '#ff00f1', '#ff00e4', '#ff00d6', '#ff00c9',
#        '#ff00bb', '#ff00ae', '#ff00a1', '#ff0093', '#ff0086', '#ff0078',
#        '#ff006b', '#ff005d', '#ff0050', '#ff0043', '#ff0035', '#ff0028',
#        '#ff001a', '#ff000d', '#ff0000', '#ff0d00', '#ff1a00', '#ff2800',
#        '#ff3500', '#ff4300', '#ff5000', '#ff5d00', '#ff6b00', '#ff7800',
#        '#ff8600', '#ff9300', '#ffa100', '#ffae00', '#ffbb00', '#ffc900',
#        '#ffd600', '#ffe400', '#fff100', '#ffff00', '#f1ff00', '#e4ff00',
#        '#d6ff00', '#c9ff00', '#bbff00', '#aeff00', '#a1ff00', '#93ff00',
#        '#86ff00', '#78ff00', '#6bff00', '#5dff00', '#50ff00', '#43ff00',
#        '#35ff00', '#28ff00', '#1aff00', '#0dff00', '#00ff00', '#00ff0d',
#        '#00ff1a', '#00ff28', '#00ff35', '#00ff43', '#00ff50', '#00ff5d',
#        '#00ff6b', '#00ff78', '#00ff86', '#00ff93', '#00ffa1', '#00ffae',
#        '#00ffbb', '#00ffc9', '#00ffd6', '#00ffe4', '#00fff1', '#00ffff']



# This color gradient contains 100 color steps.  It would be nice if I could
# add more or different colors to this list, but it's so much work to calculate
# all of the in-between shades!  If you look real close, you can see where I
# sort of gave up on it!
gradient = ['#ffe4b5', '#f5ddb2', '#ecd6af', '#e3cfad', '#d9c8aa', '#d0c1a8',
        '#c7bba5', '#beb4a3', '#b4ada0', '#aba69e', '#a29f9b', '#999999',
        '#999999', '#91969b', '#89949d', '#8292a0', '#7a90a2', '#738ea5',
        '#6b8ca7', '#648aaa', '#5c88ac', '#5586af', '#4d84b1', '#4682b4',
        '#4682b4', '#568da3', '#679893', '#78a482', '#89af72', '#9aba62',
        '#aac651', '#bbd141', '#ccdc31', '#dde820', '#eef310', '#feff00',
        '#ffff00', '#f6eb03', '#eed807', '#e6c40b', '#deb10f', '#d69e13',
        '#cd8a16', '#c5771a', '#bd641e', '#b55022', '#ad3d26', '#a52a2a',
        '#a52a2a', '#992929', '#8d2828', '#812727', '#752727', '#692626',
        '#5d2525', '#512424', '#452424', '#392323', '#2d2222', '#222222',
        '#222222', '#363031', '#4a3e40', '#5e4d50', '#725b5f', '#86696e',
        '#9a787e', '#ae868d', '#c2949c', '#d6a3ac', '#eab1bb', '#ffc0cb',
        '#ffc0cb', '#f6b1ce', '#eda2d1', '#e594d5', '#dc85d8', '#d377db',
        '#cb68df', '#c25ae2', '#b94be5', '#b13de9', '#a82eec', '#a020f0',
        '#a020f0', '#9120e5', '#8220da', '#7420cf', '#6520c4', '#5720b9',
        '#4821ae', '#3a21a3', '#2b2198', '#1d218d', '#0e2182', '#002277',
        '#002277', '#002277', '#002277', '#002277']