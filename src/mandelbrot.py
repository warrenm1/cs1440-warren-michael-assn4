#!/usr/bin/python3

## Mandelbrot Set Visualizer

from Config import config
from Gradient import gradient

images = config()

#Given a coordinate in the complex plane,
#return the iteration count of the Mandelbrot function at that point
def mandelbrotColor(c):
    """Return the color of the current pixel within the Mandelbrot set"""
    max_iterations = len(gradient)
    z = complex(0, 0)  # z0

    for i in range(max_iterations):
        z = z * z + c  # Get z1, z2, ...
        if abs(z) > 2:
            return i  # The sequence is unbounded
    return max_iterations - 1  # Indicate a bounded sequence